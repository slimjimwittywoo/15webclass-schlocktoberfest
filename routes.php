<?php

namespace App\Controllers;

use App\Views\MovieSuggestSuccessView;

$page = isset($_GET['page']) ? $_GET['page'] : 'home';

switch ($page) {
	case "home":
		
		$controller = new HomeController();
		$controller->show();

		break;

	case "about":
		$controller = new AboutController();
		$controller->show();

		break;

	case "moviesuggest":

		$controller = new MovieSuggestController();
		$controller->show();		
		
		break;
		
	case 'moviesuggestsuccess':
		$view = new MovieSuggestSuccessView();
		$view->render();

		break;
	
	default:
		echo "404";
		
}













